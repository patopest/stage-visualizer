ARG NODE_VERSION=20.12.2
ARG CADDY_VERSION=2.7.6-alpine

FROM --platform=$BUILDPLATFORM node:${NODE_VERSION} as builder

# set working directory
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install

COPY . ./
RUN npm run build


FROM caddy:${CADDY_VERSION}

WORKDIR /usr/share/caddy

# add static app
COPY --from=builder /app/dist ./