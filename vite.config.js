import { resolve } from "path";
import { defineConfig } from "vite";
import glsl from 'vite-plugin-glsl'

export default defineConfig({
    root: resolve(__dirname, "src"),
    publicDir: resolve(__dirname, "static"),
    base: './',
    server: {
        port: 3000,
    },
    build: {
        outDir: resolve(__dirname, "dist"),
        emptyOutDir: true, // Empty the folder first
        sourcemap: true // Add sourcemap
    },
    preview: {
        port: 4000,
        open: true,
    },
    plugins: [
        glsl()
    ]
})