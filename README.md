# Stage visualizer

An experiment in making a stage visualizer in WebGL with ThreeJS


## Dev

- Install dependencies

```shell
npm install
```

- Run App 

```shell
npm run dev
```
This will start a Vite development server available at [http://localhost:3000]().

- Build App

```shell
npm run build
```

## References
- Three.js [documentation](https://threejs.org/docs/index.html#manual/en/introduction/Creating-a-scene)
- 'Good enough' Volumetrics for Spotlights [article](https://john-chapman-graphics.blogspot.com/2013/01/good-enough-volumetrics-for-spotlights.html) by John Chapman